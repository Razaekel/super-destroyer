data:extend(
{ 
  {
    type = "recipe",
    name = "super-destroyer-capsule",
    enabled = false,
    energy_required = 30,
    ingredients =
    {
      {"destroyer-capsule", 4},
      {"flying-robot-frame", 4},
	  {"fusion-reactor-equipment", 2},
	  {"laser-turret", 2},
	  {"alien-artifact", 40},
	  {"processing-unit", 20},
    },
    result = "super-destroyer-capsule"
  },
}
)