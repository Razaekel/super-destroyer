data:extend(
{
  {
    type = "technology",
    name = "super-destroyer-tech",
    icon = "__base__/graphics/technology/combat-robotics.png",
    effects =
    {
      {
        type = "unlock-recipe",
        recipe = "super-destroyer-capsule"
      },
    },
    prerequisites = {"combat-robotics-3"},
    unit =
    {
      count = 500,
      ingredients =
      {
        {"science-pack-1", 1},
        {"science-pack-2", 1},
        {"science-pack-3", 1},
        {"alien-science-pack", 1}
      },
      time = 30
    },
    order = "e-p-b-b",
	upgrade = true
  },
}
)